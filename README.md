# Tutoriel sur l'utilisation de git

## Préambule

### Présentation

Ce tutoriel présente les fonctionnalités qu'on utilise le plus souvent de `git`.
Il repose sur répertoire de test contenant un script python tout simple permettant de se familiariser
avec les commandes et la stratégie de développement en branches.

Plutôt que de présenter les commandes dans un ordre de complexité, ce tutoriel fait suivre les *vraies* étapes à suivre
lors d'un développement normal sur un projet commun, qui donnent l'occasion de découvrir toutes les commandes importantes.

### Remarques

La totalité des commandes sont détaillées donc il n'y a presque rien à réfléchir ou créer soi-même, simplement suivre les étapes et
comprendre le cheminement. Comme c'est un répertoire de test n'hésite toutefois pas à t'amuser avec pour bien prendre en main
les commandes `git` ! Le tutoriel suppose que tu as déjà une connaissance minimale du principe de `git`, de `python` (et des
environnements virtuels), ainsi que de l'utilisation des lignes de commande du terminal.

Le tutoriel est pensé pour tout faire depuis un terminal, ce qui force à bien connaître les commandes brutes plutôt que celles
en clic-bouton inclues dans les IDE. On conseille tout de même par ailleurs d'installer un client interactif pour git (par exemple [GitKraken](https://www.gitkraken.com/) qui fonctionne bien, ou n'importe quel autre si tu as une préférence), cela facilite grandement la
visualisation de l'historique du répertoire et de ce qu'on modifie.

Si tu n'es pas à l'aise avec la modification de fichier depuis un terminal (via `vim`, `nano` ou autre), tu peux également modifier
les codes avec n'importe quel éditeur de texte (dans l'équipe beaucoup utilisent `Atom`), puis faire seulement les commandes `git`
depuis le terminal.

## 1. Initialisation du répertoire

### Téléchargement (clonage)

La première étape lorsqu'on rejoint un projet consiste évidemment à télécharger les codes et arriver à les faire tourner tels qu'ils sont. Pour cela, on *clone* le répertoire dans le dossier désiré. Ouvrir une fenêtre de terminal et se placer dans le bon répertoire, ici, par exemple, le dossier `Documents`.

```
cd ~/Documents

# On peut créer un dossier 'projets' qui contiendra tous nos futurs projets, avec la commande mkdir ci-dessous
mkdir projets
cd projets
```

On se trouve désormais dans le dossier `~/Documents/projets`, où l'on souhaite *cloner* le répertoire, il n'y a donc plus qu'à :

```
git clone git@bitbucket.org:Romain_dam/tutoriel_git.git
```

Tous les codes ainsi que l'historique `git` sont alors téléchargés. Par défaut le code se trouve sur la branche `master`. On reviendra sur ça plus tard. Si le projet utilise des gros fichiers (au-dessus du Mo en fait), on évite de les inclure dans l'historique
puisque cela ralentit énormément `git` qui n'est pas vraiment pensé pour les gros volumes.

### Initialisation 

*Il s'agit d'une manipulation spécifique à python ici mais on la détaille quand même. On suppose que tu as déjà installé sur ta machine le package `virtualenv` qui permet de créer un environnement virtuel.*

On se trouve déjà dans le bon dossier, donc on va simplement créer l'environnement virtuel, qu'on appelle `venv` selon la convention :

```
virtualenv  venv
```

Une fois qu'il est créé, on l'active

```
source venv/bin/activate
```

Puis on installe les packages nécessaires tels qu'ils apparaissent dans le fichier `requirements.txt`. Selon l'installation de python 3 sur ta machine, il faut utiliser `pip` ou `pip3`.

```
pip3 install -r requirements.txt
```

### Vérification

Normalement tout est bon pour pouvoir utiliser le programme. Vérifier qu'il fonctionne en le lançant (à nouveau selon
l'installation de python sur ta machine il faut utiliser `python`, `python3` ou autre)

```
python3 main.py
```

Ce qui devrait normalement afficher

```
Ha, what if I don't work as expected ?
```

Si c'est le cas, tout est bon, tout est bien installé et on va pouvoir commencer à faire des modifications !

##  2. Modifier et enregistrer les modifcations du code

### Introduction et Git flow

*A partir d'ici il peut être utile d'ouvrir le répertoire dans un client git en parallèle pour visualiser l'historique, les branches, etc. Tu peux aussi
visualiser l'arbre dans un terminal avec `git log --graph --oneline`, mais ce n'est pas non plus hyper lisible... Si tu lis l'historique sur bitbucket ou
un client git, tu verras que j'ai mis des emojis en début de commit, c'est assez pratique pour illustrer ce que fait le commit (puis c'est joli). La liste
des emojis compatibles avec bitbucket est disponible [ici](https://bitbucket.org/DACOFFEY/wiki/wiki/BITBUCKET/EMOJI/Emoji).*

On est désormais prêt pour faire des modifications. Dans le cas de ce répertoire exemple, on a trois branches `master`, `develop` et `exemple_modifs`. Les branches correspondent la plupart du temps à diverses fonctionnalités qu'on veut ajouter au code, ou à divers états du code que l'on veut différencier et garder en mémoire. On suit le procédé de développement appelé `git flow` [voir cette présentation sur les types de flux de développement](http://emmajane.github.io/gitforteams/slides/slides/workshop-oscon.html#/), composé des branches et procédures suivantes :

* `master` branche fonctionnelle à laquelle on touche directement le moins possible
* `develop` branche accueillant les développements de nouvelles fonctionnalités une fois qu'elles sont prêtes pour être testées
* `feature_xxx` branches de travail où l'on développe les nouvelles fonctionnalités avant de les fusionner sur `develop`
* `hotfix_xxx` branches courtes de correction de bug directement sur `master` ou `develop`.

**Il faut prendre l'habitude de ne jamais travailler sur `master`, à part pour corriger des bugs urgents immédiatements (hotfixes) !**

La branche `develop` est la branche qui *reçoit* les modifications en cours faites par l'équipe. En toute rigueur on n'effectue pas de modifications directement sur `develop`, mais plutôt sur une troisième branche que l'on nomme en fonction
de la fonctionnalité sur laquelle on travaille, pour ne pas créer de conflits avec les autres personnes qui elles aussi
travaillent en utilisant `develop`. On fusionnera ensuite via des *pull request* le travail de chacun·e de leurs branches respectives vers `develop` (voir fin de tuto).

> A ce stade dans le répertoire les deux branches `develop` et `master` en sont au même point. En général, puisque `develop` accueille les nouveaux développements, elle est en avance sur `master`. 

### Initialisation d'une branche de travail

Le message qui s'est affiché lorsqu'on a lancé le code est actuellement codé en dur dans le script `main.py`, ce qui n'est
pas fou. On aimerait bien qu'il soit codé *a minima* sous forme de variable, ou encore mieux dans un fichier de configuration.

On décide donc de faire les modifications nécessaires dans le code. Pour ne pas *casser* le code pour les autres qui utilisent
les branches `develop` et  `master` telles qu'elles sont, on créé donc une branche de travail. Comme on l'a dit plus haut, on créé les branches de travail plutôt sur `develop`, donc il faut dans un premier temps **se placer sur la branche `develop` plutôt que sur `master`** avec la commande suivante :

```
git checkout develop
```
 
Le terminal affiche normalement maintenant qu'on est sur `develop` entre parenthèses à côté du répertoire de travail. On peut vérifier que la manipulation a fonctionné avec la commande suivante qui indique sur quelle branche on se trouve parmi les
branches existantes.

```
git branch
```

Maintenant qu'on se trouve sur `develop`, on peut créer notre branche de travail, si possible avec un nom explicite sur ce 
qu'on compte modifier ou ajouter.

```
git checkout -b improve_default_message
```

L'option `-b` indique de créer la branche en même temps qu'on se place dessus. Autrement, on peut faire `git branch improve_default_message` pour la créer, puis `git checkout improve_default_message` pour s'y déplacer. A nouveau, vérifions
qu'on se trouve sur `improve_default_message` avec `git branch`.

### Modification mineure (un seul fichier)

On va dans un premier temps faire une modification toute simple : créer une variable pour accueiller le message par défaut plutôt que l'avoir en dur dans le `main.py`. Affichons le code tel qu'il est :

```
cat main.py
```

#### Faire la modification

On va modifier le contenu de la fonction `main()`. On peut utiliser n'importe quel éditeur de texte, voire le faire encore depuis le terminal si l'on veut (avec `nano`, `vim` ou autre). **On propose à partir d'ici les modifications à effectuer mais tu es libre de les faire comme tu veux, le cheminement est 
important mais pas les modifications elles-mêmes**

On va enlever le message en clair du bloc `if ... elif ...` et créer une variable en début de script qui sera ensuite
appelée dans `main()`.

***

A toi de jouer !

Ton fichier `main.py` pourrait par exemple ressembler à ça désormais (on a souligné les lignes modifiées avec >>>>>)

```
...


def parse_arguments():
    ...


>>>>> default_message = 'This is the new default message.'

def main(args):
    
    today_message = TimePrinter().pick_lang_message(lang = args.lang)
    
    if args.print:
        today_message.print_message(deco = args.deco)
    else:
>>>>>   print(default_message)
    
    return


if __name__ == "__main__":
    main(args = parse_arguments())

```

***

Vérifier que la modification n'a pas cassé le code en le faisant tourner :

```
python3 main.py
```

Pour afficher le message avec date du jour plutôt que le message par défaut, il suffit d'ajouter l'option `-p` :

```
python3 main.py -p
```

#### Enregistrer la modification dans un commit

Maintenant qu'on est content de notre modification, on peut la sauvegarder dans l'historique de notre branche de travail via un *commit*. Pour rappel, le commit est une sauvegarde d'un ensemble de modifications faites sur un ou plusieurs fichiers.

Une manière simple de préparer la sauvegarde des modifications est d'ajouter tout le fichier `main.py` au commit que l'on s'apprête à faire :

```
git add main.py
```

***

**Le `.gitignore`**

En faisant `git status` ensuite, on vérifie que le fichier a bien été ajouté à la sauvegarde (le commit) que l'on s'apprête à faire. Il est plutôt conseillé de faire systématiquement un `git status` avant un commit lorsqu'on n'a pas commité depuis longtemps, car on a parfois des fichiers qui s'ajoutent qu'on n'avait pas prévu (notamment les fichiers système de OS X...). Beaucoup de ces fichiers (souvent de configuration de machine locale) n'ont pas vocation à figurer dans l'historique git, puisqu'ils sont spécifiques à chaque utilisateur (et donc créent des conflits à chaque synchronisation). Ces fichiers doivent être ajouter au `.gitignore`, qui spécifie les fichiers à ignorer de l'historique git.

> **Attention** une fois qu'un fichier est ajouté à l'historique git, c'est assez dur de s'en débarasser, et c'est donc dangereux dans le cas de fichier volumineux. D'où l'intérêt de vérifier les fichiers qui vont être ajoutés avec `git status`, et de les mettre dans le `.gitignore` **avant** de faire le commit.

Le `.gitignore` consiste simplement en des lignes avec les fichiers ou dossier à exclure, avec des *wildcard* disponibles quand on veut exclure selon des motifs. `*` correspond à n'importe quelle chaîne de caractères, et `**` à la même chose mais en récursif pour parcourir toute l'arborescence d'un dossier. Pour ce répertoire, le `.gitignore` ressemble à ça :

```
.idea
venv
*.pyc
**/*.pyc

.DS_Store
**/.DS_Store
```

***

Parfois, on fait de nombreuses modifications sur un même fichier et on souhaiterait ajouter ces modifications à différents
commits plutôt que tout grouper dans un même commit, qui risquerait d'être illisible. C'est souvent assez facile avec des clients
 `git`, mais on peut également le faire facilement en terminal avec la commande `git add -p` (pour `patch`). Ici, on ferait :

```
git add main.py -p
```

Dans le terminal, git propose alors d'ajouter des *hunks* au commit. Il s'agit de groupes de lignes qui ont été modifiées dans le fichier.
En répondant oui ou non (`y/n`), on décide d'ajouter ces lignes à la sauvegarde. La bonne pratique est d'ajouter à la même sauvegarde
seulement les lignes qui correspondent à une même modification de comportement, dans le but d'avoir un commit qui ne *parle que d'une seule action*.

Typiquement, on évitera d'ajouter dans un même commit des *vraies* modifications dans une partie du code ainsi que des modifications esthétiques du genre indentation, suppression de lignes vides sur l'ensemble du script, etc., car cela nuit grandement à la lisibilité de ce que fait le commit pour les autres utilisateurs.

#### Faire le commit

Maintenant qu'on a dit à git ce que l'on voulait sauvegarder, il n'y a plus qu'à le faire ! Si on oublie l'option `-m`, git
ouvre automatiquement l'éditeur `vim` pour écrire le message, ce qui peut faire bizarre au début...

```
git commit -m "Move default message out of main definition"
```

On écrit les commits en anglais (convention), et on essaye toujours de le décrire comme une action à l'impératif, à savoir *ce que fait le commit au code*. C'est une convention, mais cela force un peu à faire des sauvegardes explicites qui font qu'on n'a pas besoin de rentrer dans le code pour comprendre ce qui a été modifié. Plus de détails sur les conventions d'écriture  de commit [ici](https://github.com/erlang/otp/wiki/writing-good-commit-messages) ou [ici](https://who-t.blogspot.com/2009/12/on-commit-messages.html).

On pourrait détailler plus ce que fait le commit en mettant plusieurs lignes (sauter une ligne entre le titre et la description) :

```
git commit -m "Move default message out of main definition

Default message was hard coded in if statements.
It is best to have it in a variable defined at the beginning of the script"
```

### Une autre modification (plusieurs fichiers)

On a fait le premier commit qui indique une modification effectuée ! Ici on n'a modifié qu'un seul fichier, mais en général une
modification modifie plusieurs fichiers. Cela ne change rien à la démarche : on dit à git quels fichiers on veut ajouter à la
sauvegarde avec une succession de `git add monfichier` (éventuellement par hunks avec l'option `-p`), puis on sauvegarde avec `git commit -m "mon message"`.

Il est d'ailleurs préférable de grouper tous les fichiers modifiés pour un même comportement dans un même commit, cela facilite grandement la navigation dans l'historique ensuite et garde la cohérence du code entre les différents fichiers.

Attention toutefois, on veut quand même qu'un commit ne corresponde qu'à une seule modification de comportement. Ainsi, il arrive souvent (en fin de journée ou si l'on oublie de commiter régulièrement) d'avoir effectué des modifications diverses, qui affectent pourtant les mêmes fichiers.

Par exemple ici, on pourrait avoir travaillé sur deux modifications :

* Mettre le message par défaut dans le fichier de configuration plutôt que comme une variable (plus facile pour la maintenance)
* Ajouter un print d'aurevoir qui indique le programme est terminé (le message figurant également dans le fichier de configuration)

Ces deux modifications ont affecté le fichier `main.py` et le fichier `config.ini`. Pour que l'historique des modifications reste clair (un commit = une modification de comportement/fonctionnement), il faut donc ajouter dans deux commits séparés les *hunks* de scripts appropriés, en utilisant 

```
git add main.py -p
git add config.ini -p
```

> Parfois git considère dans un même hunk des lignes que l'on voudrait pourtant séparer. Cela veut dire qu'elles sont très proches dans
> le code, donc bon, tant pis, on les met dans le même commit...

Dans la branche `example_modifs`, on a effectué les modifications mentionnées ci-dessus en les séparant dans deux commits. Si tu observes les commits dans bitbucket (ou dans un client git), tu verras que chaque commit modifie les deux fichiers, mais à des endroits différents.

***

A toi de jouer, reproduis ces modifications dans la branche `improve_default_message` (ou fais-en plusieurs à ta guise) qui affectent les mêmes fichiers, 
et ajoute les hunks pertinents avec `git add monfichier -p`. Il y a des chances que git ne reconnaisse pas deux lignes trop proches comme différents hunks,
auquel cas tu pourrais ajouter ligne par ligne, mais c'est hors du cadre de ce tuto (cela se fait avec `git add monfichier -i`).

***

### Et si je veux revenir en arrière ?

En général, avec git, une fois que les choses sont écrites, on évite de revenir en arrière (en tous cas au début...). Il y a toutefois un moment où ce n'est pas bien grave : si tu veux juste modifier le commit que tu viens de faire. Par exemple, tu te rends compte que tu as ajouté le `main.py` mais pas `config.ini` au commit ! Or il est pertinent que les deux soient dans le même commit. Pour cela, il suffit d'utiliser l'option `--amend`.
Dans le cas de cet exemple, cela donnerait :

```
git add main.py
git commit -m "Move default arguments to configuration file"

# Ah, forgot to add config.ini

git add config.ini
git commit --amend

```

Cela ouvre une fenêtre `vim` pour que tu puisses également changer le texte du commit si besoin (ce qui pourrait aussi être fait avec `-m` si tu ne veux pas ouvrir `vim`).

> On ne le fait pas ici par rigueur, mais en vrai, dans la plupart des cas, si tu fais des commits au fur et à mesure que tu
> codes, tu ne t'embêtes pas à écrire `git add ...` à chaque fois. Si tu es sûr·e que toutes les modifs en cours peuvent bien
> être mises dans le même commit, tu peux écrire directement `git commit -a -m 'bla bla'`. L'option `-a` ajoute au commit
> tous les documents modifiés. L'option `-a` est à utiliser avec parcimonie car c'est souvent à cause d'elle qu'on ajoute
> à  l'historique des fichiers qui ne devraient pas y être...

## 3. Publication et partage des modifications

### Démarche : les pull requests

Tu as effectué toutes les modifications que tu voulais, et tu souhaiterais désormais les partager avec le reste de l'équipe pour modifier le comportement du programme ! Cela peut arriver en fin de journée, ou en fin de sprint, bref quand un ensemble de commits affecte un même aspect du programme et
que tu veux l'intégrer dans la version fonctionnelle du code : on peut dire que ta branche est mûre et prête pour être fusionnée sur `develop`.

En général, sauf si tu es absolument sûr·e de toi (ce qui n'est **jamais** le cas sur des répertoires partagés !), on ne fusionne pas directement
la branche de travail sur `develop`, mais on va plutôt utiliser le système de *pull request*, qui permet aux autres membres de l'équipe de relire tes modifications sans
affecter `develop` pour s'assurer que la fusion est saine.

> Les *pull requests* ne sont pas une fonctionnalité de git mais plutôt des hébergeurs git (Bibucket, Github, etc.). 

L'idée d'une pull request est d'indiquer à l'équipe que tu voudrais fusionner ta branche sur `develop`, et bitbucket permet à tout le monde de voir les modifications
sur une page dédiée et de faire des commentaires sur les aspects du code sur lesquels on a des remarques, pour améliorer le code *avant* de le fusionner.
A terme, cela permet à tout le monde d'apprendre des choses, d'avoir une manière homogène de coder et de faire en sorte qu'on ne fusionne pas des bugs.

### Déclencher une pull request

Toutes tes modifications sont à ce stade consignées dans la branche `improve_default_message`. Pour décléncher une pull
request, il suffit de *pousser* ta branche vers le répertoire d'origine, soit la synchroniser avec le répertoire
distant. Cela se fait avec la commande :

```
git push --set-upstream origin improve_default_message
```

Si tu ne te souviens pas de l'ensemble, tu peux juste écrire `git push` depuis la bonne branche et git te rappellera la
syntaxe. 

Dans le message qui s'affiche, tu disposes d'un lien qui te permet d'ouvrir la pull request sur bitbucket. Ouvre le dans
un navigateur et suis les instructions : donne un nom, choisis la branche cible (souvent `develop`), choisis les
personnes qui vont relire, etc., et il n'y a plus qu'à attendre que les personnes fassent leurs commentaires.

Au fur et à mesure qu'ils font des commentaires, tu peux faire des modifications sur la branche puis les pousser, la
pull request s'actualise alors automatiquement (et supprime les commentaires liés lorsque tu as fais des modifs sur les
lignes commentées). C'est très pratique pour que la page reste lisible et que les relecteurs voient les modifications
effectuées et celles pas encore effectuées !

Une fois que tout le monde est ok, à savoir quand **toutes** les personnes en review ont *approuvé* la pull request,
l'une d'entre elles clôt la pull request en fusionnant ta branche sur la branche cible.
**Ce n'est pas la personne qui ouvre la pull request qui fait la fusion**, pour assurer une bonne procédure de relecture
et de validation.

La branche `develop` contient désormais tes modifications, et le fait d'utiliser une pull request permet de facilement
les identifier dans l'historique de la branche `develop`. Elles seront ensuite ajoutées à la version fonctionnelle du
code sur `master` (selon peu ou prou la même procédure).

> Si tu regardes l'historique de ce répertoire, c'est ce qu'on a fait pour les 4e et 5e commits, qui ont été ajoutés sur
> `master` depuis `develop` via une pull request.

> J'ai ouvert un pull request pour la branche `exemple_modifs` sur bitbucket que tu peux aller voir pour regarder la 
> tête que ça a

### Garder un répertoire à jour

#### Récupérer les versions à jour du code : *pull* et *rebase*

En général, avant de se mettre à travailler sur un code, on s'assure d'être bien à jour des modifications que les autres
ont effectuées. Le mieux pour cela est de d'abord dire à git "indique moi si des modifications ont été faites sans les
télécharger". Pour cela, on utilise `git fetch`, qui *identifie* les modifications sans les *appliquer*.

Après le `git fetch`, on regarde s'il y a des modifications à récupérer avec `git status`. Si on a envie de les
récupérer (ce qui est le cas le plus souvent), on peut alors sereinement faire un `git pull` pour les récupérer.

Normalement on ne travaille à plusieurs sur la même branche, car cela entraîne trop de risques de conflits. C'est
l'intérêt de travailler en branches puis de fonctionner par pull requests. Il arrive forcément un moment où, pendant
qu'on travaille sur notre branche de travail, des modifications aient été faites sur `develop`, et l'on souhaiterait
intégrer ces modifications à notre branche de travail plutôt que de travailler sur une ancienne version du code.

Le problème est que notre branche de travail débute plus tôt dans l'historique, et donc il n'est pas si simple d'ajouter
des modifications faites en parallèle de notre travail. C'est la fonction du *rebase* : il *remonte* le début de la
branche de travail à la version actuelle de la branche de laquelle elle a été créée. Formellement, cela consiste à
rééapliquer tous les commits de la branche de travail à partir de la dernière version de la branche de départ
(souvent `develop`). Si l'on travaille sur `working_branch` et que l'on veut récupérer des modifs faites sur `develop`,
voici  les étapes :

* Récupérer les modifs de `develop` : se mettre dessus avec `git checkout develop` puis `git pull`. La branche `develop`
	est désormais à jour des modifications des autres.
* Faire repartir la branche `working_branch` de la version actuelle de `develop` : c'est là que le *rebase* intervient.
	En effet, à la suite du *pull* sur `develop`, `working_branch` est en retard. La commande est la suivante : 
	`git rebase develop working_branch`. L'ordre est important : d'abord la branche source, puis la branche à *rebaser*.

On peut alors continuer à travailler normalement en ayant récupéré les modifications des autres. Il arrive forcément un
moment où ces modifications entreront en conflit avec celles qu'on a déjà faites. S'ouvre alors l'enfer de la gestion de
conflits sur git, qu'on ne traitera pas ici. L'idée est simplement de décider à la main, pour chacun des conflits,
quelle version on décide de garder dans notre branche.

> **Un exemple** Dans l'historique de ce répertoire, la branche `exemple_modifs` débute à partir de `develop`, mais
> d'autres modifications ont été faites sur `develop` entre temps. Pour bien récupérer ces dernières, on pourrait faire
> un rebase pour que la branche `exemple_modifs` débute sur la dernière version de `develop` : 
> `git rebase develop exemple_modifs`.

> Le site d'Atlassian contient [une bonne documentation sur le rebase](https://fr.atlassian.com/git/tutorials/rewriting-history/git-rebase)

#### Fusions après les pull requests

En général, on ferme (supprime) une branche de travail après l'avoir fusionnée sur `develop` à l'issue d'une pull
request. Il y a des cas où on ne ferme pas la branche, à savoir lorsqu'on fusionne `develop` sur `master`, par exemole.
Dans ce cas, il faut bien penser à faire une *fusion inverse* de develop sur master pour que celle-ci soit bien en date
avec *master*. Ce n'est pas indispensable de bien comprendre cela pour le moment, juste garder en tête que la gestion
de la synchronisation entre `develop` et `master` demande un peu de précautions.
 
### Situations délicates

#### Attention aux push/pull/push 

Il arrive souvent qu'on veuille pousser des modifications via un `push` sur `develop` (même si on devrait normalement
travailler sur une branche spécifique...). Git nous indique alors qu'il y a eu des modifications sur la branche distante
et qu'il faut les récupérer avant de pousser. On fait alors un `pull` comme indiqué, qui va consister à ajouter les
modifications distantes *par-dessus* les tiennes (souvent via un commit de fusion).
On peut alors faire le `push` pour apporter nos modifications à la branche d'origine.

**Il faut la plupart du temps ne surtout pas faire cette succession push/pull/push !** 

En effet, le message de git indique que des modifications ont été effectuées sur la branche distante que vous n'avez pas
prises en compte en local. En général, si vous avez un message d'erreur de ce type quand vous voulez pousser, c'est le
symptôme que quelque chose ne va pas, donc il faut bien réfléchir à quoi faire pour ne pas perturber l'historique. Les
clients git comme gitkraken après un `git fetch` sont ici d'une grande aide pour visualiser l'état de tout l'historique
(local et distant). 

L'idée derrière tout ça est de garder un historique linéaire sur la branche distante, particulièrement pour `master`, 
qui doit être vue comme une branche *intouchable* : s'il y a eu des modifs dessus que tu n'as pas, c'est à toi de te
plier d'abord à ces modifs, pas à elle de prendre d'abord en compte les tiennes. La bonne manière de faire est de les
appliquer *avant* les tiennes souvent, et non pas *après* via un pull sur votre branche.

#### Créer des branches pour sauver la mise

Une des situations du cas précédent arrive quand les historiques ont *divergé* (git le signale avec un message du type
*Your version of <branch> and the remote have diverged, please merge before pushing*). Il est très important ici de 
**ne pas tenter de résoudre la situation via des _merge_ puis des _push_**. Cela va modifier l'historique de toutes les
autres personnes utilisant le répertoire, puisque cela consigne sur la branche d'origine du répertoire des modifications
qui ne respectent plus l'ordre initial de l'historique... En bref, cela conduit souvent à dupliquer des commits et à des
intersections d'historiques en spaghettis, où l'on ne comprend plus rien à l'état souhaité du code. 

Imaginons que l'on se trouve sur `develop`, et que notre version a divergé de celle en ligne (d'autres ont fait des 
modifs, nous aussi de notre côté, du coup divergence). On ne veut pas juste pousser ou tirer en faisant les fusions 
suggérées par git. La bonne chose à faire dans ce cas-là est de créer une nouvelle branche au niveau du commit où la
branche en question a divergé. Avec `git log --graph --online` (ou mieux, un client git), on repère le code du commit
où l'on veut revenir.

Dans ce répertoire, imaginons qu'il s'agisse du commit `e86adbd` (*write incomplete version of README*). On va indiquer
à git de se placer à cet endroit de l'historique, en version *détachée* (on pourrait dire flottante : le *pointeur* de 
git n'est plus vraiment associé à une branche mais à un simple état du code à un certain moment).

```
git checkout e86adbd
```

***

A toi de jouer ! Il n'y pas de risques à faire cette manipulation pour le moment, donc ça vaut le coup de la tenter
toi-même pour voir ce qu'il se passe et retenir ! On conseille vivement d'essayer de reproduire la procédure qui suit

***

Le message suivant apparaît :

```
Note : extraction de 'e86adbd'.

Vous êtes dans l'état « HEAD détachée ». Vous pouvez visiter, faire des modifications
expérimentales et les valider. Il vous suffit de faire une autre extraction pour
abandonner les commits que vous faites dans cet état sans impacter les autres branches

Si vous voulez créer une nouvelle branche pour conserver les commits que vous créez,
il vous suffit d'utiliser « checkout -b » (maintenant ou plus tard) comme ceci :

  git checkout -b <nom-de-la-nouvelle-branche>

HEAD est maintenant sur e86adbd :pencil: Write incomplete version of README
```

Cela décrit l'état flottant dont je parlais juste au-dessus. C'est un état pratique si tu veux juster tester des 
modifications dans ton code sans effecter l'historique global ou sans avoir à les consigner dans l'historique. C'est
également comme ça que l'on crée une nouvelle branche *dans le passé*. Comme indiqué dans le message, tu n'as plus qu'à
créer une nouvelle branche comme d'habitude depuis cette situation :

```
git checkout -b fix_develop_divergence
```

Tu disposes désormais d'une nouvelle branche qui débute là où le code a divergé. Tu vas maintenant recopier un par un
sur cette branche tous les commits que tu as consignés après la divergence. C'est la fonction du `cherry-pick` : il
applique les modifs d'un commit à un autre endroit de l'historique. Comme dans cette procédure tu appliqueras à la branche
`fix_develop_divergence` les commits dans l'ordre exact où tu les avais fait sur ta version de `develop`, il n'y a 
mécaniquement aucune possibilité que le `cherry-pick` ne fonctionne pas. La commande est la suivante, depuis la branche
en cours (donc pour nous `fix_develop_divergence`) :

```
git cherry-pick <id du commit>
```

Il nous faut donc retrouver les ids de tous les commits que l'on veut appliquer. La commande `git log --graph --oneline`
permet de l'avoir facilement :

```
* ef44e30 :pencil: Add link to bitmojis
* 446fe15 :pencil: Improve commit section
* 780b469 :pencil: Commence l'ajout de liens externes
* f8d2414 :pencil: Complete tutorial in README
* e86adbd :pencil: Write incomplete version of README
*   6aeb988 (origin/master, master) Merged in develop (pull request #1)
|\  
| * f516699 :pencil: Add package requirements for virtual env
| * fc15f23 :wrench: Move default args in a .ini config file
|/  
* 6c0ed12 :sparkles: Add a main to make it scriptable
* b7d80be :sparkles: Add time_printer class
* 5e6e5f6 :tada: Initial commit
```

A ce stade on se trouve au commit `e86adbd`. On veut donc appliquer tous les commits suivants avec le `cherry-pick`,
**dans l'ordre où on les a effectués** :

```
git cherry-pick f8d2414
git cherry-pick 780b469
...
git cherry-pick ef44e30
...
```

Maintenant, notre branche `fix_develop_divergence` est exactement la même que notre branche locale `develop`. On peut
(enfin) réparer la divergence entre les deux `develop` local et distant. Pour cela, quatre étapes à suivre :

**1. Remettre la branche `develop` locale au commit de divergence** Cela consiste à annuler toutes les modifcations
faites depuis la divergence. Evidemment, ce n'est pas grave puisqu'on vient justement de les dupliquer et sauvegarder
dans la branche `fix_dvelop_divergence`. La commande est la suivante : 

```
git checkout develop  				# pour se remettre sur develop
git reset --hard e86adbd            # replacer develop local au commit de divergence
									# l'option --hard indique de ne pas garder tracer des modifications que l'on perd
```

**2. Récupérer les modifications distantes sur `develop`** Désormais, comme notre `develop` se trouve au moment de la
divergence, on peut normalement récupérer de manière linéaire les modifications distantes :

```
git status							# pour vérifier que git nous dit que develop est en retard est qu'on peut faire
									# un pull en straight-forward
git pull                            # pour appliquer les modifications distantes à develop
```

**3. Replacer la branche `fix_develop_divergence` au bon endroit** Avec le `pull` sur `develop`, la branche
`fix_develop_divergence` est en retard sur `develop`. C'est normal, puisque l'on vient d'actualiser `develop` avec des
modifications faites pendant qu'on travaillait déjà sur notre version locale. C'est là qu'intervient le `rebase` pour 
que `fix_develop_divergence` prenne sa source *à la version actuelle de `develop`*. La commande est la suivante :

```
git rebase develop fix_develop_divergence
```

Si tout se passe bien, on a désormais une branche `fix_develop_divergence` qui contient toutes les modifications que 
l'on voulait initialement apporter à `develop`, et qui prend sa source à la dernière version de `develop`. On peut 
alors fusionner la branche, **après s'être assuré d'avoir récupéré toutes les modifications distantes de `develop`, car
il peut y en avoir eu entre-temps**. S'il y en eu, on *rebase* à nouveau ! Le merge se fait comme ceci :

```
git checkout develop  				# se placer sur le branche qui reçoit la fusion
git merge fix_develop_divergence    # appliquer tous les commits de fix_develop_divergence à develop
git push                            # envoyer la nouvelle version de develop au répertoire distant
```

> Dans le cas de répertoire, les deux dernières commandes ne servent à rien car on créé artificiellement une divergence
> en reproduisant les mêmes commits. Dans la vraie vie ce sera utile...

Ne pas oublier de supprimer la branche de réparation ensuite :

```
git branch -d fix_develop_divergence
```

Et voilà, on a réussi à conserver un historique distant linéaire sans perdre de modifications ! C'est assez fastidieux,
mais une fois qu'on a pris le pli cela va vite, et c'est indispensable pour conserver des historiques lisibles. A nouveau,
**l'historique distant est toujours prioritaire sur ta version**. Au pire tu pourras toujours supprimer ton répertoire 
en local et faire un nouveau `git clone` pour récupérer le travail, alors qu'on ne peut pas remettre à zéro le répertoire
distant si facilement...

## Liste des commandes usuelles mentionnées dans le tuto :

```
# Modifications et commits en local
###################################
git checkout -b nouvelle_branche	# Crée une nouvelle branche appelée nouvelle_branche
									# depuis la branche en cours

git branch 							# affiche la branche en cours
git checkout nouvelle_branche		# bascule sur nouvelle_branche

git status							# Indique les fichiers qui ont été modifiés pour ajout dans un commit

git add monfichier					# ajoute monfichier en entier au commit à faire
git commit -m "Titre du commit" 	# effectue le commit et indique le message en même temps

git add monfichier -p  				# ajoute au commit des hunks du fichier de manière interactive

git branch -d mybranch				# supprime la branche locale
git branch -D mybranch 				# /!\ supprime la branche locale même si les modifications n'ont pas été fusionnées

# Interactions avec le répertoire distant
#########################################
git fetch							# Récupère sans les appliquer les modifications effectuées sur la branche distante
git pull							# Applique les modifications de la branche distante
git push							# Envoie les commits de la branche en cours vers la branche distante
git push origin :mybranch           # Supprime la branche mybranch sur le répertoire distant

# Commandes de modifications délicates
######################################
git cherry-pick idducommit			# Applique le commit idducommit à l'endroit actuel

git rebase master develop			# Déplace le début de la branche develop à l'état actuel de master
git merge develop 					# (depuis master) applique linéairement les commits de develop sur master
```

