from datetime import datetime


class TimePrinter():
    def __init__(self):
        self.today = datetime.now().strftime("%d-%m-%Y")
        self.message = ''
    
    def pick_lang_message(self, lang):
        if lang == 'fr':
            self.message = "Bonjour, nous sommes le %s" % self.today
        elif lang == 'en':
            self.message = 'Hey, today is %s' % self.today
        else:
            raise ValueError("lang must be one of 'fr' or 'en'")
        return self
    
    def print_message(self, deco):
        print("%s%s%s %s %s%s%s" % (deco, deco, deco, self.message, deco, deco, deco))
        return None
