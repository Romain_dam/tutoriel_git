import argparse
from configparser import ConfigParser
from src.time_printer import TimePrinter


config = ConfigParser()
config.read('config.ini')


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--lang', default = config.get('default_args', 'lang'), help = 'Language for message')
    parser.add_argument('-d', '--deco', default = config.get('default_args', 'deco'), help = 'Decoration character')
    parser.add_argument('-p', '--print', action = 'store_true', help = 'Print message')
    
    args = parser.parse_args()
    
    return args


def main(args):
    
    today_message = TimePrinter().pick_lang_message(lang = args.lang)
    
    if args.print:
        today_message.print_message(deco = args.deco)
    else:
        print("Ha, what if I don't work as expected ?")
    
    return


if __name__ == "__main__":
    main(args = parse_arguments())
